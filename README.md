JSON-JOKES
==========

List of jokes accessible from the internet for curl.


How to get some joke
--------------------

Use get_joke.py script

or

visit http://zloutek-soft.cz/json-jokes/

Tips
----

Call script like:

> ./get_joke.py | cowsay

or

> ./get_joke.py | cowsay -f tux

Thanks
------

Jokes collected from:
* http://api.icndb.com/jokes/
* https://github.com/WhatCheer/Don-Robot/blob/master/brains/jokes.json
* http://tambal.azurewebsites.net/
* https://github.com/drvinceknight/EdinburghFringeJokes/blob/master/jokes.json
